//CS 201 PENG_OUYANG 02/25/2022 section 3
package src.exams.first;

public class Pet {
    private String name;
    private int age;

    Pet(){
        name = "";
        age = 0;
    }
    Pet(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setAge(int age){
        try {
            if (age>=0){
                this.age = age;
            }
        }catch (Exception e){
            System.out.println("invalid input");
        }

    }
    public boolean equals(Pet obj){
        return this.age == obj.age && this.name.equals(obj.name);
    }
    public String toString(){
        return "This pet name is: "+this.getName()+" ,age is: " +this.getAge();
    }

}
