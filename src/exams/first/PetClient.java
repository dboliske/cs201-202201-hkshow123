//CS 201 PENG_OUYANG 02/25/2022 section 3
package src.exams.first;

public class PetClient {
    public static void main(String[] args) {

        Pet p1 = new Pet();
        Pet p2 = new Pet();
        p1.setAge(100);
        p1.setName("cat");
        System.out.println(p1.getAge());
        System.out.println(p1.getName());
        System.out.println(p2.equals(p1));
        System.out.println(p1.toString());
        System.out.println(p1.equals(p1));
    }


}
