//CS 201 PENG_OUYANG 02/25/2022 section 3
package src.exams.first;

import java.util.Scanner;

public class QuestionThree {
    public static void repetition() {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print("Please enter a number(between 1-100): ");
            int num = input.nextInt();
            if (num >= 1 && num <= 100) {
                for (int i = num; i > 0; i--) {
                    for (int j = num - i;j >0;j--){
                        System.out.print(" ");
                    }
                    for (int x = i; x > 0; x--) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }else {
                System.out.println("The number you entered too big");
            }
         } catch (
                Exception e)

        {
            System.out.println("not a valid number");
            repetition();
        }
    }

    public static void main(String[] args) {
        repetition();
    }
}
