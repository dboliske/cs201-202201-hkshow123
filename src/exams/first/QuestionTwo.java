//CS 201 PENG_OUYANG 02/25/2022 section 3
package src.exams.first;

import java.util.Scanner;

public class QuestionTwo {
    public static void selection(){
        Scanner input = new Scanner(System.in); // create scanner for user input
        try{
            System.out.print("Please enter a number: ");  //prompt for user input
            int value = input.nextInt();
            if (value%3 ==0 && value % 2== 0){
                System.out.println("foobar");
            } else if ( value %2 ==0){
                System.out.println("foo");
            }else if (value %3 ==0){
                System.out.println("bar");
            }
        } catch (Exception e){
            System.out.println("not a valid number");
            selection(); //repeat process
        }
        input.close();

    }

    public static void main(String[] args) {
        selection();
    }
}
