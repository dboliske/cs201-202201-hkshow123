//CS 201 PENG_OUYANG 02/25/2022 section 3
package src.exams.first;

import java.util.Scanner;

public class QuestionFour {
    public static void printWords() {
        Scanner input = new Scanner(System.in);
        String[] values = new String[5];
        String[] sameValue = new String[5];
        int count = 0;
        int num = 0;
        for (int i = 0; i < 5; i++) {
            System.out.print("Enter a word: ");
            values[i] = input.nextLine();
        }
        for (int i = 0; i < values.length; i++) {
            for (int j = i + 1; j < values.length; j++) {

                if (values[i].equals(values[j])) {
                    count++;
                }
            }
            if (count == 1) {
                System.out.println(values[i]);
            }
            count = 0;
            input.close();
        }

    }

    public static void main(String[] args) {
        printWords();
    }
}
