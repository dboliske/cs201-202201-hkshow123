//CS 201 PENG_OUYANG 02/25/2022 section 3
package src.exams.first;

import java.util.Scanner;

public class QuestionOne {

    public static void printChar(){
        Scanner input = new Scanner(System.in); //create Scanner for user input
        try {
            System.out.print("Please enter a Integer number: "); //prompt for a number
            int value = input.nextInt();
            char result = (char)value; //convert the int to char
            System.out.println(result);
        } catch (Exception e){
            System.out.println("not a valid value");
            printChar();
        }
        input.close();
    }

    public static void main(String[] args) {
        printChar();
    }

}
