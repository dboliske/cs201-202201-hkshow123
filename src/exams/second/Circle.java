//CS201 PENG OUYANG 04/29/2022
package exams.second;

public class Circle extends Polygon{
    private double radius;

    public Circle() {
        super();
        this.radius = 1;
    }

    public Circle(String name, double radius) {
        super(name);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        if (radius >0){
            this.radius = radius;
        }
    }

    @Override
    public String toString() {
        return "radius: " + radius +
                ", name: " + name ;
    }

    @Override
    public double area() {
        //A = pi r^2
        return Math.PI*radius*radius;
    }

    @Override
    public double perimeter() {
        //C=2πr
        return 2.0*Math.PI*radius;
    }

}
