//CS201 PENG OUYANG 04/29/2022
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class Question3 {

    public static void main(String[] args) {
        ArrayList<Integer> nums = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.print("Please enter a number: ");
            String in = input.next();
            if (in.equalsIgnoreCase("done")) {
                break;
            } else {
                try {
                    int num = Integer.parseInt(in);
                    if (nums.size() > 0) {
                        int pointer = nums.size();
                        int i = nums.size() - 1;
                        while (i >= 0) {
                            if (nums.get(i) > num) {
                                i--;
                                pointer--;
                                if (i < 0) {
                                    nums.add(pointer, num);
                                    break;
                                }
                            } else {
                                nums.add(pointer, num);
                                break;
                            }
                        }

                    } else {
                        nums.add(num);
                    }

                } catch (Exception e) {
                    System.out.println("I don't understand");
                }
            }
        }
        System.out.println("The minimum is: "+ nums.get(0));
        System.out.println("The maximum is: "+ nums.get(nums.size()-1));
    }

}
