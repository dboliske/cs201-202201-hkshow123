# Final Exam

## Total

92/100

## Break Down

1. Inheritance/Polymorphism:    19/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  4/5
2. Abstract Classes:            20/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  5/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        14/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  4/5

## Comments

1. The constructor of classroom didn't check the validation of seats. -1
2. ok
3. Error when there is no input value. -1
4. ok
5. didn't implement the jump search algorithm recursively -5; 
   didn't allow the user to search for a value;-1
