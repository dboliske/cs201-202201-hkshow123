//CS201 PENG OUYANG 04/29/2022
package exams.second;

public class ComputerLab extends Classroom{

    private boolean computers;


    public ComputerLab() {
        super();
        this.computers = false;
    }
    public ComputerLab(boolean computers) {
        super();
        this.computers = computers;
    }

    public ComputerLab(String building, String roomNumber, int seats, boolean computers) {
        super(building, roomNumber, seats);
        this.computers = computers;
    }

    public boolean hasComputers(boolean computers) {
        return computers;
    }

    public void setComputers(boolean computers) {
        this.computers = computers;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", computers: " + computers ;
    }

}
