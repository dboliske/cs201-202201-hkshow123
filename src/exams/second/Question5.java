//CS201 PENG OUYANG 04/29/2022
package exams.second;

public class Question5 {
    public static void main(String[] args) {
        double[] myArr = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};


        System.out.println(jumpSearch(myArr, 0.577));
        System.out.println(jumpSearch(myArr, 3.142));
        System.out.println(jumpSearch(myArr, 1.304));
        System.out.println(jumpSearch(myArr, 7));


    }


    public static int jumpSearch(double[] myArr, double target) {
        int step = (int) Math.sqrt(myArr.length - 1);
        int prev = 0;
        while (prev < myArr.length) {
            if (myArr[step] < target) {
                prev = step;
            } else if (myArr[step] >= target) {
                for (int i = prev; i <= step; i++) {
                    if (myArr[i] == target)
                        return i;
                }
            }
            step += (int) Math.sqrt(myArr.length - 1);
            if (step > myArr.length) {
                return -1;
            }
        }
        return -1;
    }
}
