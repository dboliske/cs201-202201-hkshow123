//CS201 PENG OUYANG 04/29/2022
package exams.second;

public class Classroom {
    protected String building;
    protected String roomNumber;
    private int seats;

    public Classroom() {
        this.building = "";
        this.roomNumber = "";
        this.seats = 0;
    }

    public Classroom(String building, String roomNumber, int seats) {
        this.building = building;
        this.roomNumber = roomNumber;
        this.seats = seats;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {

        if (seats > 0) {
            this.seats = seats;
        }else {
            System.out.println("Invalid number");
        }
    }

    @Override
    public String toString() {
        return "building: " + building +
                ", roomNumber: " + roomNumber +
                ", seats: " + seats;
    }
}
