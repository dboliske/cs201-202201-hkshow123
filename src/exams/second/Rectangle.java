//CS201 PENG OUYANG 04/29/2022
package exams.second;

public class Rectangle extends Polygon{
    private double width;
    private double height;

    public Rectangle() {
        super();
        this.width = 1;
        this.height = 1;
    }


    public Rectangle(String name,double width, double height) {
        super(name);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        if (width>0){
            this.width = width;
        }
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        if (height>0){
            this.height = height;
        }
    }

    @Override
    public String toString() {
        return "name: '" + name+
                ", width: " + width +
                ", height: " + height;
    }

    @Override
    public double area() {
        return width*height;
    }

    @Override
    public double perimeter() {
        return (width+height)*2;
    }
}
