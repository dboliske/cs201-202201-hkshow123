//CS201 PENG OUYANG 04/29/2022

package exams.second;

public abstract class Polygon {
    protected   String name;

    public Polygon(){
        this.name = "";
    }
    public Polygon(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name: " + name ;
    }

    public abstract double area();

    public abstract double perimeter();

}
