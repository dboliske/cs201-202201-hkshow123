//CS201 PENG OUYANG 04/29/2022
package exams.second;


public class Question4 {
    public static void main(String[] args) {

        String[] myArr ={"speaker", "poem", "passenger", "tale", "reflection",
                "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
        String[] newArr = selectionSort(myArr);
        for (String str:newArr){
            System.out.println(str);
        }
    }


    public static String[] selectionSort(String[] myArr){
        for (int i = 0; i < myArr.length -1; i++) {
           int min = i;
            for (int j = i+1; j < myArr.length; j++) {
                if (myArr[min].compareTo(myArr[j]) > 0){
                    min = j;
                }
            }
            String temp = myArr[i];
            myArr[i] = myArr[min];
            myArr[min] = temp;
        }
        return myArr;
    }


}

