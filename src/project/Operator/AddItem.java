//CS201 PENG OUYANG 04/21/2022
/* this class includes two methods,
    inputInfo method : by ask user enter the information to add product manually;
    addExist method: user can select the index of product to duplicate products.
 * */
package project.Operator;

import project.Products.Product;
import project.view.MainMenu;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class AddItem {

    public static void inputInfo(Scanner input) throws IOException {
        boolean loopFlag = true;
        ArrayList info = new ArrayList<>();

        Boolean isExpire = false;
        boolean restriction = false;
        FileWriter fileWriter = new FileWriter("src/project/stock2.csv", true);

        while (loopFlag) {
            System.out.print("Please enter the name you want to add: ");
            String name = input.next();
            info.add(name);
            try {
                System.out.print("Please enter the price: ");
                double price = input.nextDouble();
                info.add(price);
                System.out.print("Does it has age restriction ? (y/n): ");
                String flag = input.next();
                if (flag.equalsIgnoreCase("y")) {
                    restriction = true;
                    System.out.print("Please enter age: ");
                    int age = input.nextInt();
                    info.add(age);
                }
                System.out.print("Does it has expire day ? (y/n): ");
                String expire = input.next();
                if (expire.equalsIgnoreCase("y")) {
                    isExpire = true;
                    System.out.print("Please enter year: ");
                    int year = input.nextInt();
                    System.out.print("Please enter the month of year: ");
                    int month = input.nextInt();
                    System.out.print("Please enter the day of month: ");
                    int day = input.nextInt();

                    LocalDate expireDay = LocalDate.of(year, month, day);

                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
                    formatter.format(expireDay);
                    info.add(formatter.format(expireDay));
                }
                System.out.print("Do you want to duplicate this item ? (y/n):  ");
                String isDuplicate = input.next();
                int j = 1;
                if (isDuplicate.equalsIgnoreCase("y")) {
                    System.out.print("How many items do you want to duplicate:  ");
                    j = input.nextInt();
                    if (j <1){
                        System.out.println("Please enter a proper number");
                        return;
                    }
                }

                String line = "";
                for (int i = 0; i < info.size(); i++) {
                    line += (i == info.size() - 1 ? info.get(i).toString() : (info.get(i).toString() + ","));
                }
                for (int k = 0; k < j; k++) {
                    System.out.println(line);
                    fileWriter.write(line + "\n");
                }

                System.out.print(" Press 'Y' to continue add items, any other key to exit:  ");
                String isContinue = input.next();
                if (!isContinue.equalsIgnoreCase("y")) {
                    break;
                }

            } catch (Exception e) {
                loopFlag = false;
                System.out.println("Sorry,Invalid input!");
                break;
            }
        }

        fileWriter.close();

    }
    public static ArrayList<Product> addExist(ArrayList<Product> inventory,Scanner input) throws IOException {
        MainMenu.displayProducts(inventory);
        System.out.print("Please choice item you want to add: ");
        int choice;
        try {
            choice = input.nextInt();
        }catch (Exception e){
            System.out.println("-----------Invalid input------Please enter Product ID------------");
            return inventory;
        }
        if (choice >=0) {
            System.out.print("How many items do you want to add: ");
            int num = input.nextInt();
            for (int i = 0; i < num; i++) {
                inventory.add(inventory.get(choice - 1));
            }
        }
        DataSaver.dataSaver(inventory);
        System.out.println("Add successful! ");

        return inventory;
    }

}
