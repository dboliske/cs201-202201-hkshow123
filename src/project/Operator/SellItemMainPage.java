//CS201 PENG OUYANG 04/21/2022
/*
    this is the main page of sell item function, The method sellItem will list all the products with its index. prompt for us
    er make a choice
 * */
package project.Operator;


import project.Products.Product;
import project.view.MainMenu;

import java.util.ArrayList;
import java.util.Scanner;

public class SellItemMainPage {
    public static ArrayList<Product> sellItem(ArrayList<Product> inventory, ArrayList<Product> shoppingCart, Scanner input) {
        System.out.println("=============================Products list=========================");
        MainMenu.displayProducts(inventory);
        System.out.println("-------------------------------------------------------------------");
        System.out.print("please enter the product id to add to shopping cart or press 'n' to exit: ");
        int index = input.nextInt() - 1;
        SellRestrictItem.sellItemRestrict(inventory, shoppingCart, input,index);
        return inventory;
    }
}
