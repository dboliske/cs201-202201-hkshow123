//CS201 PENG OUYANG 04/21/2022
/*
    dataSaver method: this method main function is convert the instance to string and use fileWriter to write the strings
    into file.
 * */

package project.Operator;

import project.Products.*;

import java.io.FileWriter;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class DataSaver {


    public static void dataSaver(ArrayList<Product> inventory) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
        FileWriter cleaner = new FileWriter("src/project/stock.csv");
        cleaner.write("");
        cleaner.close();
        FileWriter  fileWriter = new FileWriter("src/project/stock.csv",true);

        for (int i = 0; i < inventory.size(); i++) {
            if(inventory.get(i) instanceof Stationery ){
                fileWriter.write(inventory.get(i).getName() + "," + inventory.get(i).getPrice()+"\n");
            }else if(inventory.get(i) instanceof Fruit){
                fileWriter.write(inventory.get(i).getName() + "," + inventory.get(i).getPrice() + ","
                        + ((Fruit) inventory.get(i)).getExpireDay().format(formatter)+"\n");
            }else if (inventory.get(i) instanceof Alcohol){
                fileWriter.write(inventory.get(i).getName() + "," + inventory.get(i).getPrice() + ","
                        + ((Alcohol) inventory.get(i)).getAge()+"\n");
            }else if (inventory.get(i) instanceof Others){
                fileWriter.write((inventory.get(i).getName() + "," + inventory.get(i).getPrice() + ","
                        + ((Others) inventory.get(i)).getAge()) + "," +
                        ((Others) inventory.get(i)).getDate().format(formatter)+"\n");
            }else {
                System.out.println("Unknown data type");
            }
        }
        fileWriter.flush();
        fileWriter.close();
    }
}
