//CS201 PENG OUYANG 04/21/2022
/* this file is for user modify the product information; It contains 5 method in this file;
    modifyItem method : this is the menu of modify function;
    modifyName method :this method is use for user modify name。
    modifyPrice method :this method is use for user modify price.
    modifyAge method :this method is use for user modify restriction age;


 * */

package project.Operator;

import project.Products.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;

public class ModifyItem {

    public static ArrayList<Product> modifyItem(ArrayList<Product> inventory, Scanner input,int index) throws IOException {
//        Inventory.checkInventory(inventory);
//        int index  =SearchItem.search(inventory,input);
        for (int i = 0; i < inventory.size(); i++) {
            System.out.println( "Product ID: <"+i+ "> "+inventory.get(i).toString());
        }
        if (index >-1){
            System.out.println("======================================================");
            System.out.println(inventory.get(index).toString());
            System.out.println("1.Modify name");
            System.out.println("2.Modify price");
            System.out.println("3.Modify restriction age");
            System.out.println("4.Modify expire day");
            System.out.println("6.Exit");
            System.out.println("======================================================");
            System.out.print("Please choice: ");
            String choice = input.next();
            System.out.println("");
            switch (choice){
                case "1":
                    modifyName(inventory,input,index);
                    break;
                case "2":
                    modifyPrice(inventory,input,index);
                    break;
                case "3":
                    modifyAge(inventory,input,index);
                    break;
                case "4":
                    modifyExpireDay(inventory,input,index);
                case "6":
                    break;
            }


        }else {
            System.out.println("Wrong information entered");
        }
        return inventory;
    }
    public static ArrayList<Product> modifyName(ArrayList<Product> inventory,Scanner input,int index) throws IOException {
        String oldName =  inventory.get(index).getName();
        System.out.print("Please enter a new name: ");
        String newName = input.next();
        int count = 0;
        for (int i = 0; i < inventory.size(); i++) {
            if (inventory.get(i).getName().equals(oldName)){
                inventory.get(i).setName(newName);
                count++;
            }
        }
        System.out.println("< "+count+" >items has been modified");
        DataSaver.dataSaver(inventory);
        return inventory;
    }

    public static ArrayList<Product> modifyPrice(ArrayList<Product> inventory,Scanner input,int index) throws IOException {
        String Name =  inventory.get(index).getName();
        System.out.print("Please enter a new price: ");
        double newPrice = input.nextDouble();
        if (newPrice < 0){
            System.out.println("Invalid price entered");
            return inventory;
        }
        int count = 0;
        for (int i = 0; i < inventory.size(); i++) {
            if (inventory.get(i).getName().equals(Name)){
                inventory.get(i).setPrice(newPrice);
                count++;
            }
        }
        System.out.println("< "+count+" >items has been modified");
        DataSaver.dataSaver(inventory);
        return inventory;
    }
    public static ArrayList<Product> modifyAge(ArrayList<Product> inventory,Scanner input,int index) throws IOException {
        String Name =  inventory.get(index).getName();
        System.out.print("Please enter a new age: ");
        int newAge = input.nextInt();
        if (newAge < 0 || newAge >199){
            System.out.println("Invalid age entered");
            return inventory;
        }
        int count = 0;
        for (int i = 0; i < inventory.size(); i++) {
            if (inventory.get(i).getName().equals(Name)){
                if (inventory.get(i) instanceof Stationery){
                    Alcohol item = (Alcohol)inventory.get(i);
                    item.setAge(newAge);
                }else if(inventory.get(i) instanceof Fruit) {
                    Others item = (Others) inventory.get(i);
                    item.setAge(newAge);
                }else if (inventory.get(i) instanceof Alcohol)
                {
                    ((Alcohol) inventory.get(i)).setAge(newAge);
                }else if (inventory.get(i) instanceof Others){
                    ((Others) inventory.get(i)).setAge(newAge);
                }
                count++;
            }
        }
        System.out.println(count+" items has been modified");
        DataSaver.dataSaver(inventory);
        return inventory;
    }
    public static ArrayList<Product> modifyExpireDay(ArrayList<Product> inventory,Scanner input,int index) throws IOException {
        String Name =  inventory.get(index).getName();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
        LocalDate date = null;
        try {
            System.out.print("Please enter the year: ");
            int year = input.nextInt();
            System.out.print("Please enter the month: ");
            int month = input.nextInt();
            System.out.print("Please enter the day: ");
            int day = input.nextInt();
            date= LocalDate.of(year,month,day);

        }catch (Exception e){
            System.out.println("Please enter a correct date");
        }
        LocalDate newDate = LocalDate.parse(date.format(formatter));
        int count = 0;
        for (int i = 0; i < inventory.size(); i++) {
            if (inventory.get(i).getName().equals(Name)){
                if (inventory.get(i) instanceof Stationery){
                    Fruit item = (Fruit)inventory.get(i);
                    item.setExpireDay(newDate);
                }else if(inventory.get(i) instanceof Fruit) {
                    Fruit item = (Fruit)inventory.get(i);
                    item.setExpireDay( newDate);
                }else if (inventory.get(i) instanceof Alcohol)
                {
                    Others item = (Others) inventory.get(i);
                    item.setDate(newDate);
                }else if (inventory.get(i) instanceof Others){
                    ((Others) inventory.get(i)).setDate(newDate);
                }
                count++;
            }
        }
        System.out.println(count+" items has been modified");
        DataSaver.dataSaver(inventory);
        return inventory;
    }


}
