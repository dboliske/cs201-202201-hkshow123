//CS201 PENG OUYANG 04/21/2022
/* this class includes two methods,
   menu method : this is the search function's submenu
    add method: user can use the search result to add and  duplicate products.
 * */
package project.Operator;

import project.Products.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class SearchFuncMenu {

    public static ArrayList<Product> menu(ArrayList<Product> inventory, Scanner input) throws IOException {
        System.out.println("============================Welcome========================");
        Inventory.checkInventory(inventory);
        System.out.println("======================================================");
        int index =  SearchItem.search(inventory,input);
        System.out.println(inventory.get(index).toString());
        if (index ==-1){
            System.out.println("Item not find");
            return inventory;
        }
        System.out.println("-----------------------------------------");
        System.out.println("1.add this item: ");
        System.out.println("2.Modify this item: ");
        System.out.println("3.Delete this item: ");
        System.out.println("4.Exit");
        System.out.print("Please choice: ");
        String choice = input.next();
        System.out.println("");
        switch (choice){
            case "1":
                add(inventory,index,input);
                break;
            case "2":
                ModifyItem.modifyItem(inventory,input,index);
                for (int i = 0; i < inventory.size(); i++) {
                    System.out.println( "Product ID: <"+i+ "> "+inventory.get(i).toString());
                }
                break;
            case "4":
                break;
        }
        return inventory;
    }
    public static ArrayList<Product> add(ArrayList<Product> inventory,int index,Scanner input) throws IOException {
        Product temp = inventory.get(index);
        System.out.println("How many do you want to add? : ");
        System.out.print("Number = ");
        try {
            int num = input.nextInt();
            if (num >=0) {
                for (int i = 0; i < num; i++) {
                    inventory.add(temp);
                }
            }
            DataSaver.dataSaver(inventory);
            System.out.println(temp.getName()+ " has been added");
        }catch (Exception e){
            System.out.println("Please enter a proper number");
        }
        return inventory;
    }
}
