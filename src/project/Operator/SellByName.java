//CS201 PENG OUYANG 04/21/2022
/*
   searchByName method: this is subfunction of searchItem,the main function is display the target on the screen.
 * */
package project.Operator;

import project.Products.Product;

import java.util.ArrayList;
import java.util.Scanner;

public class SellByName {
    public static ArrayList<Product> searchByName(ArrayList<Product> inventory, Scanner input, ArrayList<Product> shoppingCart) {
        int index = SearchItem.search(inventory, input);
        if (index != -1) {
            System.out.println(inventory.get(index).toString());
        }else {
            System.out.println("Item doesn't exist");
            return inventory;
        }
        System.out.print("Do you want add this item to your shopping cart ? (y/n ): ");
        String choice = input.next();
        if (index != -1) {
            if (choice.equalsIgnoreCase("y")) {
                SellRestrictItem.sellItemRestrict(inventory,shoppingCart,input,index);
            }
        } else {
            System.out.println("Item not find");
        }
        return inventory;
    }

}
