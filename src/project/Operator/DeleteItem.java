//CS201 PENG OUYANG 04/21/2022
/* this is a delete function class file,it has 3 methods,
    deleteItem method :  this is the delete function menu.
    deleteById method : this method is use for user delete item by use product id;
    deleteByName method : this method is use for user delete item by enter the product name;
 * */

package project.Operator;

import project.Products.Product;

import java.util.ArrayList;
import java.util.Scanner;

public class DeleteItem {
    public static void main(String[] args) {

    }
    public static ArrayList<Product> deleteItem(ArrayList<Product> inventory, Scanner input){
        Inventory.checkInventory(inventory);
        System.out.println("======================================================================");
        System.out.println("1.Delete item by Product ID");
        System.out.println("2.Delete item by name: ");
        System.out.println("3.Exit");
        System.out.print("Please choice: ");
        String choice = input.next();
        System.out.println("");
        switch (choice){
            case "1":
                deleteById(inventory,input);
                break;
            case "2":
                deleteByName(inventory,input);
                break;
            case "3":
                break;
        }
        return inventory;
    }
    public static ArrayList<Product> deleteById(ArrayList<Product> inventory,Scanner input){
        for (int i = 0; i < inventory.size(); i++) {
            System.out.println("Product ID: "+(i + 1) + " " + inventory.get(i).toString());
        }
        System.out.print("Please enter the Product ID you want to delete: ");
        try{
            int index = input.nextInt() -1;
            String name = inventory.get(index).getName();
            inventory.remove(index);
            System.out.println("Product: "+name+" has been deleted");

        }catch (Exception e){
            System.out.println("Invalid input");
        }
        return inventory;
    }

    public static ArrayList<Product> deleteByName(ArrayList<Product> inventory,Scanner input){
        Inventory.checkInventory(inventory);
        int index = SearchItem.search(inventory,input);
        if (index != -1){
            String name = inventory.get(index).getName();
            inventory.remove(index);
            System.out.println("Product: "+name+" has been deleted");
        }else {
            System.out.println("Item doesn't find");
        }
        return inventory;
    }
    
}
