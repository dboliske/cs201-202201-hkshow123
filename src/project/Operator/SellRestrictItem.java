//CS201 PENG OUYANG 04/21/2022
/*
    sellItemRestrict method : this is a submethod of sellItemMainPage, if the item has age restriction, this method
    will ask user birthday information.
 * */

package project.Operator;

import project.Products.Alcohol;
import project.Products.Product;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class SellRestrictItem {

    public static ArrayList<Product> sellItemRestrict(ArrayList<Product> inventory,
                                                      ArrayList<Product> shoppingCart,
                                                      Scanner input,
                                                      int index) {
        try {

            String name = inventory.get(index).getName();
            if (inventory.get(index).hasRestriction()) {
                System.out.print("Please enter what year  were you born: ");
                int year = input.nextInt();
                System.out.print("Please enter what was the month were you born: ");
                int month = input.nextInt();
                System.out.print("Please enter the day of the month: ");
                int day = input.nextInt();
                LocalDate nowDate = LocalDate.now();
                LocalDate age = LocalDate.of(year, month, day);
                Alcohol obj = (Alcohol) inventory.get(index);
                if (inventory.get(index).hasRestriction()) {
                    if (age.plusYears(obj.getAge()).isAfter(nowDate)) {
                        System.out.println("Sorry, " + obj.getName() + " has age restriction," +
                                " you must " + obj.getAge() + "year old");
                    } else {
                        shoppingCart.add(inventory.get(index));
                        inventory.remove(index);
                        System.out.println(name + " : has been add to your shopping cart");
                    }
                }
            } else {
                shoppingCart.add(inventory.get(index));
                inventory.remove(index);
                System.out.println(name + " : has been add to your shopping cart");

            }

        } catch (Exception e) {
            System.out.println("Invalid input!");

        }

        return inventory;
    }
}
