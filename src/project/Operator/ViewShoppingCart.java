//CS201 PENG OUYANG 04/21/2022
/*
     viewShoppingCart method : this method helps user check what items have been added into shopping cart
 * */
package project.Operator;

import project.Products.Product;
import project.view.CustomerMenu;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class ViewShoppingCart {
    public static ArrayList<Product> viewShoppingCart(ArrayList<Product> shoppingCart, ArrayList<Product> inventory, Scanner input) throws IOException {
        System.out.println("<===================Your shopping cart===================>");
        DecimalFormat df = new DecimalFormat("#.##");
        double total = 0;
        for (int i = 0; i < shoppingCart.size(); i++) {
            System.out.println((i + 1) + " :" + shoppingCart.get(i).toString());
            total += shoppingCart.get(i).getPrice();
        }
        System.out.println("You have " + shoppingCart.size() + " items in your shopping cart, " +
                "Total is: $" + df.format(total) + " .");
        System.out.println("-------------------------------------------------------------------");
        CustomerMenu.cartMenu(shoppingCart, inventory, input);
        return shoppingCart;
    }
}
