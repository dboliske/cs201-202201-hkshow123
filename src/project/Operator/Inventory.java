//CS201 PENG OUYANG 04/21/2022
/* this class file is use for user check inventory.
    checkInventory method : this method use a map to list quantity of each product.
 * */
package project.Operator;

import project.Products.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Inventory {

    public static Map<String,Integer> checkInventory(ArrayList<Product> inventory){
        ArrayList<String> name = new ArrayList<>();
        Map<String,Integer> itemName = new HashMap<>();
        for (int i = 0; i < inventory.size(); i++) {
            name.add(inventory.get(i).getName());
        }

        for (String stringName: name){
            if (itemName.containsKey(stringName)){
                itemName.put(stringName,itemName.get(stringName).intValue()+1);
            }else {
                itemName.put(stringName,1);
            }

        }

        Iterator<String> iterator = itemName.keySet().iterator();
        while (iterator.hasNext()){
            String productName = iterator.next();
            System.out.println("Name: "+ productName + "   total: "+ itemName.get(productName));
        }
        return itemName;
    }

}


