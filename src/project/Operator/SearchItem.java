//CS201 PENG OUYANG 04/21/2022
/*
    the search method will ask user enter info and return the result. if find item it will return index else return -1
 * */
package project.Operator;

import project.Products.Product;

import java.util.ArrayList;
import java.util.Scanner;

public class SearchItem {

    public static int search(ArrayList<Product> inventory, Scanner input){
        System.out.print("Please enter the name: ");
        int index = -1;
        String name = input.next();
        for (int i = 0; i < inventory.size(); i++) {
            if (inventory.get(i).getName().equalsIgnoreCase(name)){
                index = i;
                break;
            }
        }
        return index;
    }

}
