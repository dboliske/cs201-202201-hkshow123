//CS201 PENG OUYANG 04/21/2022
/*
 readFromFile method : this method main function is to scan data from stock.csv file and identify their regularity,
 use the data to create different instance.
 * */
package project.Operator;

import project.Products.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class DataReader {

    public static ArrayList<Product> readFromFile() throws FileNotFoundException {
        ArrayList<Product> inventory = new ArrayList<>();
        File f = new File("src/project/stock.csv");
        Scanner input = new Scanner(f);
        //bottle of water,1.00,04 /29 /2023
        while (input.hasNextLine()) {
            String[] data = input.nextLine().split(",");

            if (data.length == 3) {
                if (data[2].length() == 10) {
                    double price = Double.parseDouble(data[1]);
                    String[] dataS = data[2].split("/");
                    int year = Integer.parseInt(dataS[2]);
                    int month = Integer.parseInt(dataS[0]);
                    int day = Integer.parseInt(dataS[1]);

                    LocalDate expireDay = LocalDate.of(year, month, day);
                    Fruit fruit = new Fruit(data[0], price, expireDay);
                    inventory.add(fruit);
                } else if (data[2].length() == 2) {
                    // cold meds,24.99,18
                    double price = Double.parseDouble(data[1]);
                    int age = Integer.parseInt(data[2]);
                    Alcohol alcohol = new Alcohol(data[0], price, age);
                    inventory.add(alcohol);
                }
            } else if (data.length == 4) {
                //kdk,20.0,30,2020-02-20
                double price = Double.parseDouble(data[1]);
                int age = Integer.parseInt(data[2]);
                if (data[3].length() == 10) {
                    String[] dataS = data[3].split("/");
                    int year = Integer.parseInt(dataS[2]);
                    int month = Integer.parseInt(dataS[0]);
                    int day = Integer.parseInt(dataS[1]);

                    LocalDate expireDay = LocalDate.of(year, month, day);
                    Others others = new Others(data[0], price, age, expireDay);
                    inventory.add(others);
                }
            } else if (data.length == 2) {
                double price = Double.parseDouble(data[1]);
                Stationery stationery = new Stationery(data[0], price);
                inventory.add(stationery);
            }


        }
        return inventory;
    }
}
