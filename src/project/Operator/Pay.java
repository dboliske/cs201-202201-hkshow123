//CS201 PENG OUYANG 04/21/2022
/*  the pay method is a checkout function.
 * */
package project.Operator;

import project.Products.Product;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Pay {
    public static ArrayList<Product> pay(ArrayList<Product> shoppingCart, ArrayList<Product> inventory) throws IOException {
        double total = 0;
        DecimalFormat df = new DecimalFormat("#.##");
        for (int i = 0; i < shoppingCart.size(); i++) {
            total += shoppingCart.get(i).getPrice();
        }
        System.out.println("You paid for " + shoppingCart.size() + " items in your shopping cart, " +
                "Total is: $" + df.format(total) + " .");
        shoppingCart.clear();
        DataSaver.dataSaver(inventory);
        return inventory;
    }}
