//CS201 PENG OUYANG 04/21/2022
/*
    this is a subclass of Product for all fresh products which have expiration time
 * */
package project.Products;

import java.time.LocalDate;

public class Fruit extends Product {
    private LocalDate expireDay;

    public Fruit(String name, double price, LocalDate expireDay) {
        super(name, price);
        this.expireDay = expireDay;
    }

    public LocalDate getExpireDay() {
        return expireDay;
    }

    public void setExpireDay(LocalDate expireDay) {
        this.expireDay = expireDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fruit)) return false;
        Fruit obj = (Fruit) o;
        if (this.getName().equals(obj.getName()) && this.getPrice() == obj.getPrice()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return super.toString() + "expire day: <"+expireDay+" >";
    }
}
