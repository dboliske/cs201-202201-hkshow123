//CS201 PENG OUYANG 04/21/2022
/*
    this is a subclass of Product for products which don't have aga restriction and expiration time
 * */
package project.Products;


public class Stationery extends Product{
    public Stationery(String name, double price) {
        super(name, price);
    }

}
