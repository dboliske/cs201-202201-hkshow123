//CS201 PENG OUYANG 04/21/2022
/*
    this is a father class for all products
 * */
package project.Products;

import java.util.Objects;


public class Product {

    private String name;
    private double price;
    private boolean restriction = false;


    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean hasRestriction(){
        return restriction;
    }

    public void setRestriction(boolean restriction){
        this.restriction = restriction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) && Objects.equals(price, product.price);
    }

    @Override
    public String toString() {
        return "product name: << " +getName()+" >> price: < "+getPrice()+" > ";
    }
}
