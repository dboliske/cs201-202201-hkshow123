//CS201 PENG OUYANG 04/21/2022
/*
    this is a subclass of Product for all other non-alcohol products but have age restriction and expiration time products
 * */
package project.Products;

import java.time.LocalDate;

public class Others extends Product {

    private boolean restriction = true;
    private int age;
    private LocalDate ExpireDay;

    public Others(String name, double price, int age, LocalDate date) {
        super(name,price);
        this.age = age;
        this.ExpireDay = date;
    }


    public boolean isRestriction() {
        return restriction;
    }

    public void setRestriction(boolean restriction) {
        this.restriction = restriction;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getDate() {
        return ExpireDay;
    }

    public void setDate(LocalDate date) {
        this.ExpireDay = date;
    }
    @Override
    public String toString() {
        return super.toString()+"restriction age: <" + age +" > "+"expire day: <"+ExpireDay+" >";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Others)) return false;
        if (!super.equals(o)) return false;
        Others other = (Others) o;
        return this.getAge() == other.getAge() && this.getName().equals(other.getName())
                &&this.getPrice()==other.getPrice();
    }
}
