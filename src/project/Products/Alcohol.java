//CS201 PENG OUYANG 04/21/2022
/*
    this is a subclass of  Product for all alcohol products which have age restriction but no expiration time
 * */
package project.Products;

public class Alcohol extends Product {
    private final boolean restriction = true;
    private int age;

    public Alcohol(String name, double price,int age) {
        super(name, price);
        this.age = age;
    }

    public boolean hasRestriction() {
        return restriction;
    }

    public void  setAge(int age){
        this.age = age;
    }
    public int getAge(){
        return age;
    }

    @Override
    public String toString() {
        return super.toString()+"restriction age: <" + age +" >";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Alcohol)) return false;
        if (!super.equals(o)) return false;
        Alcohol alcohol = (Alcohol) o;
        return this.getAge() == alcohol.getAge() && this.getName().equals(alcohol.getName())
                &&this.getPrice()==alcohol.getPrice();
    }

}
