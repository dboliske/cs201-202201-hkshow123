//CS201 PENG OUYANG 04/21/2022
/* this is a submenu of MainMenu for access all admin function
 *
 * */
package project.view;

import project.Operator.*;
import project.Products.Product;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class AdminMenu {
    public static void main(String[] args) throws FileNotFoundException {

    }

    public static void menu(ArrayList<Product> inventory, Scanner input) throws IOException {
        ArrayList<Product> shoppingCart = new ArrayList<>();
        boolean flag = true;
            System.out.println("============================Welcome========================");
            Inventory.checkInventory(inventory);
            System.out.println("======================================================");
            System.out.println("1.add item");
            System.out.println("2.search item");
            System.out.println("3.delete item");
            System.out.println("4.sell item");
            System.out.println("5.Modify item");
            System.out.println("6.Exit");
            System.out.println("======================================================");
            System.out.print("Please choice: ");
            String choice = input.next();
            System.out.println("");
            switch (choice) {
                case "1":
                    addFunctionMenu.addProduct(inventory, input);
                    break;
                case "2":
                    SearchFuncMenu.menu(inventory,input);
                    break;
                case "3":
                    DeleteItem.deleteItem(inventory,input);
                    DataSaver.dataSaver(inventory);
                    break;
                case "4":
                    CustomerMenu.customerMenu(inventory,input);
                    break;
                case "5":
                    for (int i = 0; i < inventory.size(); i++) {
                        System.out.println( "Product ID: <"+i+ "> "+inventory.get(i).toString());
                    }
                    int index =  SearchItem.search(inventory,input);
                    ModifyItem.modifyItem(inventory,input,index);
                    break;

            }
        }

}
