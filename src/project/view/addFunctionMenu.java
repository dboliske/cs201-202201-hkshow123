//CS201 PENG OUYANG 04/21/2022
/* this is a submenu of AdminMenu for add product function
*
* */

package project.view;
import project.Operator.AddItem;
import project.Operator.Inventory;
import project.Products.Product;

import java.io.IOException;
import java.util.*;


public class addFunctionMenu {

    public static ArrayList<Product> addProduct(ArrayList<Product> inventory, Scanner input) throws IOException {

        System.out.println("1.add item that already in system: ");
        System.out.println("2.add item manually: ");
        System.out.println("3.exit ");
        System.out.print("Please choice: ");
        String choice = input.next();
        System.out.println("");
        switch (choice) {
            case "1":
                AddItem.addExist(inventory, input);
                break;
            case "2":
                Inventory.checkInventory(inventory);
                AddItem.inputInfo(input);
                break;
        }
        return inventory;
    }


}




