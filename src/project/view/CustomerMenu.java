//CS201 PENG OUYANG 04/21/2022
/* this is a submenu of MainMenu for access all customer function
 *
 * */
package project.view;

import project.Operator.*;
import project.Products.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class CustomerMenu {

    public static ArrayList<Product> customerMenu(ArrayList<Product> inventory, Scanner input) throws IOException {
        ArrayList<Product> shoppingCart = new ArrayList<>();

        boolean flag = true;
        while (flag) {
            System.out.println("<----------------------------------------------------------------------->");
            System.out.println("1.Products list: ");
            System.out.println("2.Search Item: ");
            System.out.println("3.view your shopping cart: ");
            System.out.println("4.check out: ");
            System.out.println("5.exit");
            System.out.print("Please choice: ");
            String choice = input.next();
            switch (choice) {
                case "1":
                    SellItemMainPage.sellItem(inventory, shoppingCart, input);
                    break;
                case "2":
                    Inventory.checkInventory(inventory);
                    SellByName.searchByName(inventory, input, shoppingCart);
                    break;
                case "3":
                    ViewShoppingCart.viewShoppingCart(shoppingCart, inventory, input);
                    break;
                case "4":
                    Pay.pay(shoppingCart, inventory);
                    break;
                case "5":
                    flag = false;
                    break;
                default:
                    System.out.println("Backing to menu!");
            }
        }
        return inventory;
    }

    public static ArrayList<Product> cartMenu(ArrayList<Product> shoppingCart, ArrayList<Product> inventory, Scanner input) throws IOException {
        System.out.println("1.MainMenu");
        System.out.println("2.delete product from shopping cart");
        System.out.println("3.check out");
        System.out.print("Please choice: ");
        String choice = input.next();
        switch (choice) {
            case "1":
                break;
            case "2":
                System.out.print("Please enter the product id you want to delete: ");
                try {
                    int id = input.nextInt();
                    inventory.add(shoppingCart.get(id - 1));
                    String name = shoppingCart.get(id - 1).getName();
                    shoppingCart.remove(id - 1);
                    System.out.println(name + " :has been deleted!");
                    return inventory;
                } catch (Exception e) {
                    System.out.println("Invalid input");
                }
            case "3":
                int num = shoppingCart.size();
                Pay.pay(shoppingCart, inventory);
                DataSaver.dataSaver(inventory);
                System.out.println("<<------ "+ num + " items have been-Paid---------->>");
                break;
            default:
                System.out.println("Sorry,Invalid input");
        }
        return shoppingCart;
    }

}