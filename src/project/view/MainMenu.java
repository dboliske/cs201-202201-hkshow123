//CS201 PENG OUYANG 04/21/2022
/* this is a main Menu to access this program.
 *
 * */

package project.view;

import project.Operator.DataReader;
import project.Products.Product;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MainMenu {
    public static void main(String[] args) throws FileNotFoundException {
        while (true) {
            ArrayList<Product> inventory = DataReader.readFromFile();
            Scanner input = new Scanner(System.in);
            System.out.println("=============Welcome =================");
            System.out.println("1.I am a customer");
            System.out.println("2. I am a administrator");
            System.out.print("Please choice: ");
            try {
                int choice = input.nextInt();
                if (choice == 1) {
                    CustomerMenu.customerMenu(inventory, input);
                } else if (choice == 2) {
                    AdminMenu.menu(inventory, input);
                } else {
                    System.out.println("Please choice 1 or 2");
                }
            } catch (Exception e) {
                System.out.println("Invalid input!");
            }

        }
    }



    public static void adminMenu() {
        System.out.println("============================Welcome========================");
    }

    public static void displayProducts(ArrayList<Product> inventory) {
        for (int i = 0; i < inventory.size(); i++) {
            System.out.println("Product ID: "+(i + 1) + " " + inventory.get(i).toString());
        }

    }
}
