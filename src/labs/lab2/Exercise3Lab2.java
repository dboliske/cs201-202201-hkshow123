//CS 201 PENG_OUYANG 02/01/2022
package labs.lab2;

import java.util.Scanner;

public class Exercise3Lab2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //create scanner
        int choice = 0;
        do{
            System.out.println("----------------------------");   //print menu
            System.out.println("1.Say hello.");
            System.out.println("2.Addition.");
            System.out.println("3.Multiplication.");
            System.out.println("4.Exit.");
            System.out.print("Please choice: "); //prompt for user input
            choice = input.nextInt();

            if(choice == 1){
                System.out.println("Hello!");
                } else if (choice == 2) {
                System.out.print("Please enter first number: ");
                double firstNum = input.nextDouble();
                System.out.print("Please enter the second number: ");
                double secNum = input.nextDouble();
                System.out.println("-> " + firstNum + " + " + secNum + " = " + (firstNum + secNum));
            } else if (choice == 3) {
                System.out.print("Please enter first number: ");
                double first = input.nextDouble();
                System.out.print("Please enter the second number: ");
                double second = input.nextDouble();
                System.out.println("-> " + first + " * " + second + " = " + (first * second));
            } else if (choice ==4) {
                System.out.println("Thank you, goodbye!");
                break;

            }

        } while(choice != 4);


    }
}
