//CS 201 PENG_OUYANG 02/01/2022
package labs.lab2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class averageGrade {
    public static void main(String[] args) {
        Scanner input =new Scanner(System.in); // create scanner
        int count = 0;   // count how many values
        double sum = 0;
        double num =0;  // to save user entered value
        do {
            System.out.print("Please enter the grade you want to calculate, enter '-1' to finish: ");
            num = input.nextDouble();
            if (num == -1){
                break;
            } else{
            sum +=num;
            count++;}

        } while (num != -1 );
        DecimalFormat df = new DecimalFormat("#.##");   //decimal format
        System.out.println("average is : "+df.format(sum/count)); //print out the result
    }
}
