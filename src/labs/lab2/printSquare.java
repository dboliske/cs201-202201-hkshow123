//CS 201 PENG_OUYANG 02/01/2022
package labs.lab2;

import java.util.Scanner;

public class printSquare {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //create scanner for user input
        System.out.print("Please enter a number: "); //prompt for a number
        int num = input.nextInt();
        for(int i=0;i <num;i++) {  //print columns
            for(int j=0;j<num;j++){  //print rows
                System.out.print("* ");
            }
            System.out.println(""); //change line
        }
    }
}
