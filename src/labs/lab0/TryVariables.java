//PENG OUYANG ,CS201,01/23/2022
package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		short small = 25; //did not declare variable
		byte tiny = 19;

		float f = .0925F; //missing a ';'
	    double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		System.out.println("small is " + small); //first letter should be capitalized
		System.out.println("tiny is " + tiny); //misspelling "tine"
		System.out.println("f is " + f);
		System.out.println("decimal is " + decimal); //use '.' instead of  ','
		System.out.println("largeDouble is " + largeDouble);
		System.out.println("character is "+ character); //missing a '+' sign
		System.out.println("t is " + t);

	}

}