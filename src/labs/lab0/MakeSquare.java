//PENG OUYANG ,CS201,01/23/2022
package labs.lab0;
/*
 * for(square_height){
 *	 for(square_length){
 * 		print(*);
 * 		print(space;}
 *	  change to next lane;	 		
 * }
 **/

public class MakeSquare {

	public static void main(String[] args) {
		
		for(int i = 9;i>0;i--) {
			//the height of the square
			for(int x =10;x>0;x--) {
				// the length of the square
				System.out.print('*');
				System.out.print(' ');
				// increase lineSpacing
			}
			System.out.println();
			//change to next line
		}
	}

}
