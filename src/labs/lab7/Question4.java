//CS 201 PENG_OUYANG 02/01/2022 section 3
package src.labs.lab7;

import java.util.Scanner;

public class Question4 {
    public static void main(String[] args) {
        String[] arr1 = {"c", "html", "java", "python", "ruby", "scala"};
        Scanner input = new Scanner(System.in);

        System.out.print("Please enter a value: ");
        String value = input.nextLine();
        int position = binarySearch(arr1, value);
        input.close();
        System.out.println(value+" is at arr1["+position+"]");
    }

    public static void printPosition() {

    }

    public static int binarySearch(String[] myArr, String value) {
        int start = 0;
        int end = myArr.length;
        int pos = -1;
        boolean found = false;
        while (!found && (start != end)) {
            int middle = (start + end) / 2;
            if (myArr[middle].equalsIgnoreCase(value)) {
                found = true;
                pos = middle;
            } else if (myArr[middle].compareToIgnoreCase(value) > 0) {
                end = middle;
            } else {
                start = middle;
            }
        }
        return pos;
    }
}
