//CS 201 PENG_OUYANG 02/01/2022 section 3
package src.labs.lab7;

public class Question2 {
    public static void main(String[] args) {
        String[] arr1 = {"cat", "fat", "dog", "apple", "bat", "egg"};
        for (int i = 0; i < insertionSort(arr1).length; i++) {
            System.out.println(arr1[i]);
        }
    }

    public static String[] insertionSort(String[] myArr){

        for (int i = 1; i < myArr.length; i++) {
            String temp = myArr[i];
            int j = i-1;
            while (j>=0 && myArr[j].compareToIgnoreCase(temp) >0){
                myArr[j+1] = myArr[j];
                j--;
            }
            myArr[j+1] = temp;
        }
        return myArr;
    }

}
