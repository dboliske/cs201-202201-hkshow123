//CS 201 PENG_OUYANG 02/01/2022 section 3
package src.labs.lab7;

public class Question1 {
    public static void main(String[] args) {
        int[] arr1 = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
        BubbleSort(arr1);
        for (int j : arr1) {
            System.out.print(j);
        }
    }

    public static int[] BubbleSort(int[] myArr){
        for (int i = myArr.length-1; i >0;i--){
            for (int j = 0; j < i; j++) {
                if (myArr[j] > myArr[j+1]){
                    int temp = myArr[j+1];
                    myArr[j+1] = myArr[j];
                    myArr[j] = temp;
                }

            }
        }
        return myArr;
    }
}
