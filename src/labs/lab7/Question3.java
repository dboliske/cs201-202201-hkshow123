//CS 201 PENG_OUYANG 02/01/2022 section 3
package src.labs.lab7;

public class Question3 {
    public static void main(String[] args) {
        double[] arr1 = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        for (int i = 0; i < selectionSort(arr1).length; i++) {
            System.out.println(arr1[i]);
        }
    }

    public static double[] selectionSort(double[] myArr){
        int min = 0;
        for (int i = 0; i <myArr.length -1 ; i++) {
            for (int j = i; j < myArr.length; j++) {
                if (myArr[min] >myArr[j]){
                    min = j;
                }
            }
            double temp = myArr[i];
            myArr[i] = myArr[min];
            myArr[min] = temp;

        }
        return myArr;
    }

}
