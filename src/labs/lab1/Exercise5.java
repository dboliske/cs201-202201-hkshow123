// cs201 Peng Ouyang, lab1 exercise5
package labs.lab1;

import java.util.Scanner;

public class Exercise5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //create scanner for user input
        System.out.print("enter the length: "); //prompt for length
        double length = input.nextDouble();
        System.out.print("enter the width: ");  //prompt for width
        double width = input.nextDouble();
        System.out.print("enter the depth: ");  //prompt for depth
        double depth = input.nextDouble();
        double area = (length * depth) * 2 + (length * width) * 2 + (depth * width) * 2; // the formula to calculate surface area
        System.out.println("--->to make this box we need:( " + area + " )sqrt wood!");
    }
}
/* the test plan include 3 sets of different data type, integer and float
1.
enter the length: 10
enter the width: 20
enter the depth: 30
--->to make this box we need:( 2200.0 )sqrt wood!
2.
enter the length: 20.5
enter the width: 30
enter the depth: 30.5
--->to make this box we need:( 4310.5 )sqrt wood!
3.
enter the length: 10000
enter the width: 2000
enter the depth: 3000
--->to make this box we need:( 1.12E8 )sqrt wood!


*/