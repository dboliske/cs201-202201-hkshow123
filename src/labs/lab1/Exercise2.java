// cs201 Peng_Ouyang lab1 exercise 2
package labs.lab1;

import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // create scanner for user input
        System.out.print("Enter what year was your father born: ");
        int fatherYear = Integer.parseInt(input.nextLine());
        System.out.print("what year were you born in: ");
        int yourYear = Integer.parseInt(input.nextLine());
        System.out.print("How tall are you in inch: ");
        int height = Integer.parseInt(input.nextLine());
        input.close();  // release memory
        int fatherAge = 2022 - fatherYear;  // father's age
        int yourAge = 2022 - yourYear;  //your age
        int heightInFeet = height / 12; //convert inch to feet
        int heightInInch = height % 12; // remainder convert to inch

        System.out.println("1. Your age subtracted from your father's age: " + (fatherAge - yourAge));
        System.out.println("Your birth year multiplied by 2: " + yourYear / 2 );
        System.out.println("Convert your height in inches to cms:" + (height * 2.54) + "cm");
        System.out.println("Convert your height in inches to feet and inches: " + heightInFeet + " Feet " + heightInInch + " Inch"  );
    }
}
