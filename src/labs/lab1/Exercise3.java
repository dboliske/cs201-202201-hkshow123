// cs201 Peng Ouyang lab1 exercise3
package labs.lab1;

import java.util.Locale;
import java.util.Scanner;

public class Exercise3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //create scanner for user input
        System.out.print("Enter your first name: "); //prompt for first name
        char firstName = input.nextLine().toUpperCase(Locale.ROOT).charAt(0); //convert the first letter to upper case
        input.close(); //close scanner
        System.out.println("Your first initial is: "+firstName); //print out the result

    }
}