// cs201 Peng Ouyang, lab1 exercise6
package labs.lab1;

import java.util.Scanner;

public class Exercise6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //create scanner for user input
        System.out.print("Please enter the length(in inch): "); //prompt for user input
        double length = input.nextDouble(); 
        double lengthInCm = length * 2.54; // convert to cm
        System.out.println(length + " inch is: "+ lengthInCm +" cm"); //print out the result in cm
    }
}

/*
Please enter the length(in inch): 10
10.0 inch is: 25.4 cm

Please enter the length(in inch): 9
9.0 inch is: 22.86 cm

Please enter the length(in inch): 1000
1000.0 inch is: 2540.0 cm

Please enter the length(in inch): -5
-5.0 inch is: -12.7 cm
*/