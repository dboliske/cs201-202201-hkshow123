// cs201 Peng Ouyang, lab1 exercise4
package labs.lab1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //create scanner for user input
        boolean running = true; //initialize flag
        //print user menu
        while (running) {
            System.out.println("1. Fahrenheit to Celsius ");
            System.out.println("2. Celsius to Fahrenheit ");
            System.out.println("3. Exit ");
            System.out.print("choice: ");  //prompt for a choice
            int choice = input.nextInt();

            DecimalFormat df = new DecimalFormat("#.##"); //format output
            if (choice == 1) {
                System.out.print("Please enter the temperature in Fahrenheit: ");
                double tempInFa = input.nextDouble();
                double tempInCel = ((tempInFa - 32) / 1.8);
                System.out.println("->" + df.format(tempInFa) + " Fahrenheit degree is ( " + df.format(tempInCel) + " ) Celsius degree");
            } else if (choice == 2) {
                System.out.print("Please enter the temperature in Celsius: ");
                double tempInCel = input.nextDouble();
                double tempInFa = (tempInCel * 1.8) + 32;
                System.out.println("->" + df.format(tempInCel) + " Celsius degree is ( " + df.format(tempInFa) + " ) Fahrenheit degree");
            } else if (choice ==3) {
                running = false;
                System.out.println("Thank you, goodbye!");
            } else {
                System.out.println("soryy, it is not an option!");
            }
        }
    }
}
/*
test results：
I tested the negative and positive numbers, the results are satisfied my expectation.

    1.Please enter the temperature in Fahrenheit: 104
      104 Fahrenheit degree is ( 40 ) Celsius degree

    2.Please enter the temperature in Celsius: 37
      37 Celsius degree is ( 98.6 ) Fahrenheit degree

    3.Please enter the temperature in Fahrenheit: 50
      50 Fahrenheit degree is ( 10 ) Celsius degree

    4.Please enter the temperature in Fahrenheit: -100
      -100 Fahrenheit degree is ( -73.33 ) Celsius degree
*/
