// Peng Ouyang cs201 lab3 excercise 1
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;

public class averageGrade {
    public static void main(String[] args) throws IOException {
        File f =new File("src/labs/lab3/grades.csv"); // create file source path
        Scanner input = new Scanner(f); //create scanner read data from file
        int count = 0; // count how many students
        double sum = 0; // the grade's sum of all student
        while (input.hasNextLine()){
            String line = input.nextLine(); // read each line
            String[] values = line.split(",");
            double score = Double.parseDouble(values[1]); // convert string type to double
            sum = sum + score;
            count++;
        }
        DecimalFormat df = new DecimalFormat("#.##");  // format decimal
        System.out.println("the average grade is: " + df.format(sum/count));  //print out the result
        input.close();
    }
}
