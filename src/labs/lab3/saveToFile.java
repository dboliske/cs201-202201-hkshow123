package labs.lab3;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class saveToFile {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in); //create scanner for user input
        double[] num = new double[0];
        int count = 0;
        String value;
         do{
             System.out.print("please enter a number or enter 'done' to end: "); //prompt for user input
             value = input.nextLine();
             if (value.equalsIgnoreCase("done")){
                 break;
             }
             else if (!value.isEmpty()) {
                 double[] extra = new double[num.length + 1];     //create the array
                 for (int i = 0; i < num.length; i++) {
                     extra[i] = num[i];
                 }
                 num = extra;
                 extra = null;
                 try {
                     num[count] = Double.parseDouble(value);  //assign input value to array
                     count++;
                 } catch (Exception e) {
                     System.out.println("Invalid Input");
                 }

             }
        }while (!value.equalsIgnoreCase("done"));

        System.out.print("please enter a file name: ");    //prompt for file name
        String fileName = input.nextLine();
        OutputStream  os = new FileOutputStream("src/labs/lab3/" +fileName); //create writer
        for (int i=0;i <num.length;i++) {
            byte[] buffer = String.valueOf(num[i]).getBytes(StandardCharsets.UTF_8); //convert the array to bytes
            os.write(buffer);          //write values to file
            os.write("\r\n".getBytes(StandardCharsets.UTF_8));   //change line
        }
        os.close();
        input.close();

    }

}
