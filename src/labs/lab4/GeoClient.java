//CS 201 PENG_OUYANG 02/01/2022 section 3
package labs.lab4;

import labs.lab4.GeoLocation;

public class GeoClient {
    public static void main(String[] args) {
        GeoLocation g = new GeoLocation();
        System.out.println(g.toString());
        GeoLocation g2 = new GeoLocation(3,7);
        System.out.println(g2.toString());
        g2.setLat(5.0);
        g2.setLng(6.0);
        System.out.println(g2.getLat()+" "+g2.getLng());
        System.out.println(g2.vailLat(3));
        System.out.println(g2.vailLng(-800));
        GeoLocation g3 =new GeoLocation(5.0,6.0);
        System.out.println(g3.equals(g));

    }
}
