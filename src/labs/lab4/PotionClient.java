//CS 201 PENG_OUYANG 02/01/2022 section 3
package labs.lab4;

public class PotionClient {
    public static void main(String[] args) {
        Potion p = new Potion();
        Potion p1 = new Potion("test",8);
        Potion p2 = new Potion("test2",10);
        System.out.println(p1.getName());
        System.out.println(p1.getStrength());
        p.setName("test");
        p.setStrength(8);
        System.out.println(p.toString());
        System.out.println(p.equals(p1));
        System.out.println(p.equals(p2));
        System.out.println(p.validStrength(10));
    }
}
