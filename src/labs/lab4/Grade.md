# Lab 4

## Total

24/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        7/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        7/8
  * Application Class   1/1
* Documentation         0/3

## Comments

1. No documentation explaining what the code is intended for
2. None of the non-default constructors validate the parameters before setting the instance variables
