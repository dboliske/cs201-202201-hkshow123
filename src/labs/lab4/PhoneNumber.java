//CS 201 PENG_OUYANG 02/01/2022 section 3
package labs.lab4;

public class PhoneNumber {
    private String countryCode;
    private String areaCode;
    private String number;

    public PhoneNumber() {
        countryCode = "00";
        areaCode = "000";
        number = "0000000000";
    }

    public PhoneNumber(String country, String area, String num) {
        countryCode = country;
        areaCode = area;
        number = num;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public String getNumber() {
        return number;
    }

    public void setCountryCode(String country) {
        try {
            if ((country.length() >= 2) && ((Integer.parseInt(country)) % 1 == 0)) {
                this.countryCode = country;
            }
        } catch (Exception e) {
            System.out.println("not a valid number");
        }
    }

    public void setAreaCode(String area) {
        try {
            if ((area.length() == 3) && (Integer.parseInt(area) % 1 == 0)) {
                this.areaCode = area;
            }
        } catch (Exception e) {
            System.out.println("not a valid number");
        }
    }

    public void setNumber(String num) {
        try {
            if ((num.length() == 7) && (Integer.parseInt(num) % 1 == 0)) {
                this.number = num;
            }
        } catch (Exception e) {
            System.out.println("not a valid number");
        }
    }

    public String toString() {
        return ("Country code:" + this.countryCode + " Area code: " + this.areaCode + " Number: " + this.number);
    }

    public boolean vailAreaCode(String area) {
        try {
            return area.length() == 3 && Integer.parseInt(area) % 1 == 0 ;
        } catch (Exception e) {
            System.out.println("Invalid Number");
            return false;
        }
    }

    public boolean vailNumber(String num) {
        try {
            return num.length() == 7 && Integer.parseInt(num) % 1==0;
        } catch (Exception e){
            System.out.println("Invalid number");
            return false;
        }
    }
    public boolean equals(PhoneNumber p){
        return
        (this.countryCode.equals(p.countryCode) ) && (this.areaCode.equals(p.areaCode) ) && (this.number.equals(p.number));

    }

}
