//CS 201 PENG_OUYANG 02/01/2022 section 3
package labs.lab4;

public class GeoLocation {
    private double lat;
    private double lng;

    public GeoLocation() {
        lat = 0.0;
        lng = 0.0;
    }

    public GeoLocation(double lat,double lng) {
        this.lat = lat;
        this.lng = lng;
    }
    public double getLat(){
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLat(double lat) {
        if (lat >=-90 && lat <=90) {
            this.lat = lat;
        }
    }

    public void setLng(double lng) {
        if ( lng >= -180 && lng <=180) {
            this.lng = lng;
        }
    }
    public String toString(){
        return ((lat <0?("0"+lat):lat )+" "+ (lng <0?("0"+lng):lng));
    }
    public boolean vailLat(double lat){
      return lat >=-90 && lat <=90;
    }
    public boolean vailLng(double lng) {
        return lng >=-180 && lng<=180 ;
    }
    public boolean equals(GeoLocation g) {
        return  ((g.lat == this.lat) && (g.lng == this.lng)) ;

    }


}
