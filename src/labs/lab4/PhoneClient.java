//CS 201 PENG_OUYANG 02/01/2022 section 3
package labs.lab4;

import labs.lab4.PhoneNumber;

public class PhoneClient {
    public static void main(String[] args) {
        PhoneNumber p = new PhoneNumber();
        System.out.println(p.toString());
        PhoneNumber p1 = new PhoneNumber();
        p1.setCountryCode("114");
        System.out.println(p1.getCountryCode());
        p1.setAreaCode("222");
        System.out.println(p1.getAreaCode());
        p1.setNumber("7777717");
        System.out.println(p1.getNumber());
        boolean res = p1.vailAreaCode("221");
        System.out.println(res);
        System.out.println(p1.vailNumber("12345678"));
        System.out.println(p1.toString());
        boolean e = p1.equals(p);
        System.out.println(e);
    }
}
