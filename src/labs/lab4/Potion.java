//CS 201 PENG_OUYANG 02/01/2022
package labs.lab4;

public class Potion {
    private  String name;
    private  double strength;
    public Potion(){
        name ="";
        strength =0.0;
    }
    public Potion(String name,double strength) {
        this.name = name;
        this.strength = strength;
    }
    public String getName(){
        return name;
    }
    public double getStrength(){
        return strength;
    }
    public void setName(String name) {
        if (name.length() >0){
            this.name = name;
        }
    }
    public void setStrength(double strength){
        if (strength >=0 && strength <=10){
            this.strength = strength;
        }
    }
    public String toString(){
        return "name is: " + this.name +" :strength is: " + this.strength;
    }
    public boolean validStrength(double strength){
        return strength >=0 && strength<=10;
    }
    public boolean equals(Potion p){
        return  (this.name.equals(p.name) && this.strength == p.strength);
    }
}
