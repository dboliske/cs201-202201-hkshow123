package src.labs.lab6;

public  class  customer {
    private String name;
    private String favoriteFood;

    public customer(){

    }

    public customer(String name, String favoriteFood) {
        this.name = name;
        this.favoriteFood = favoriteFood;
    }

    public String getName() {
        return name;
    }

    public String getFavoriteFood() {
        return favoriteFood;
    }

    public void setFavoriteFood(String favoriteFood) {
        this.favoriteFood = favoriteFood;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void toDo(){
        System.out.println(this.name + "bought" + this.favoriteFood);
    }


}
