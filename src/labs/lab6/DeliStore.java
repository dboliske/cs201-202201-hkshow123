package src.labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliStore {

    public static void menu(Scanner input, ArrayList<customer> queue) {
        boolean flag = true;
        String choice;
        do {
            System.out.println("============Welcome to deli store=============");
            System.out.println("1.Add customer to queue");
            System.out.println("2.Help customer");
            System.out.println("3.Exit");
            System.out.print("Please choice: ");
            choice = input.nextLine();
            switch (choice) {
                case "1":
                    addCustomer(input, queue);
                    break;
                case "2":
                    dequeue(queue);
                    break;
                case "3":
                    input.close();
                    flag = false;
                    break;
                default:
                    System.out.println("Invalid input");
            }
        } while (flag);

    }

    public static ArrayList<customer> addCustomer(Scanner input, ArrayList<customer> queue) {
        System.out.println("--------------------------------------------------");
        System.out.print("Please enter your name: ");
        String name = input.nextLine();
        System.out.print("What is your favorite food: ");
        String food = input.nextLine();
        customer newCustomer = new customer(name, food);
        queue.add(newCustomer);
        System.out.println(name + " you are the > " + (queue.indexOf(newCustomer) + 1) + " < in the queue.");
        System.out.println("The queue length is:-------->  " + queue.size());
        return queue;
    }

    public static ArrayList<customer> dequeue(ArrayList<customer> queue) {
        if (queue.size() > 0) {
            System.out.println(queue.get(0).getName() + " bought " + queue.get(0).getFavoriteFood());
            queue.remove(0);
            System.out.println("The queue length is:-----------> " + queue.size());
            System.out.println("--------------------Thank you------------------");
            return queue;
        } else {
            System.out.println("No customer in the queue..");
            return queue;
        }
    }

    public static void main(String[] args) {
        ArrayList<customer> queue = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        menu(input, queue);
    }

}
