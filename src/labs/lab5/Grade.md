# Lab 5

## Total

15/20

## Break Down

CTAStation

- Variables:                    1/2
- Constructors:                 1/1
- Accessors:                    1/2
- Mutators:                     1/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                2/2
- Loops to display menu:        1/2
- Displays station names:       1/1
- Displays stations by access:  1/2
- Displays nearest station:     2/2
- Exits                         1/1

## Comments
- Didn't implement methods as required. -3
- In CTAStopApp, when runing display stations by access, the program reports error: java.util.NoSuchElementException -2;
