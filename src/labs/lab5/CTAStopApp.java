package src.labs.lab5;

import javax.imageio.IIOException;
import java.io.File;
import java.util.Locale;
import java.util.Scanner;

public class CTAStopApp {
    public static void main (String[] args) throws IIOException {
        CTAStation[] cta = readFile();
        menu(cta);

    }
    public static void menu(CTAStation[] cta){
        Scanner input = new Scanner(System.in);
        boolean flag = true;
        do {
            System.out.println("-------------------------------------------------");
            System.out.println("1.Display Station Names ");
            System.out.println("2.Display Stations with/without Wheelchair access");
            System.out.println("3.Display Nearest Station");
            System.out.println("4.Exit");
            System.out.print("Your choice: ");
            int choice = input.nextInt();
            System.out.println("-------------------------------------------------");
            switch (choice) {
                case 1 -> displayStationNames(cta);
                case 2 -> displayByWheelChair(cta);
                case 3 -> displayNearest(cta);
                case 4 -> flag = false;
            }
        }while (flag);
        input.close();
    }

    public static CTAStation[] readFile() {
        CTAStation[] cta = new CTAStation[50];
        try {
            File f = new File("src/labs/lab5/CTAStops.csv");
            Scanner input = new Scanner(f);
            input.nextLine();
            int count = 0;
            try {
                while (input.hasNextLine()){
                    String[] values = input.nextLine().split(",");
                    //Name,Latitude,Longitude,Location,Wheelchair,Open
                    String name = values[0];
                    double lat = Double.parseDouble(values[1]);
                    double lng = Double.parseDouble(values[2]);
                    String location = values[3];
                    boolean wheelChair= Boolean.parseBoolean(values[4]);
                    boolean open = Boolean.parseBoolean(values[5]);
                    CTAStation ctaObj = new CTAStation(name,lat,lng,location,wheelChair,open);
                    cta[count] = ctaObj;
                    count++;
                }

                CTAStation[] temp = new CTAStation[count];
                for (int i = 0; i < count; i++) {
                    temp[i] = cta[i];
                }
                cta = temp;
                temp = null;

            } catch (Exception e){
                System.out.println(" no more information");
            }
        input.close();
        }catch (Exception e) {
            System.out.println("file not found");
        }

    return cta;
    }

    public static void displayStationNames(CTAStation[] cta){
        if (cta.length > 0) {
            for (int i = 0; i < cta.length; i++) {
                System.out.println((i+1)+": "+cta[i].getName());
            }
        }
    }

    public static void displayByWheelChair(CTAStation[] cta){
        Scanner input = new Scanner(System.in);
        System.out.println("Do you need wheelchair access?");
        System.out.print("Enter 'y' for 'yes', 'n' for 'no': ");
        String choice = input.nextLine();
        try {
            switch (choice.toLowerCase(Locale.ROOT)) {
                case "y":
                    int count = 0;
                    for (int i = 0; i < cta.length; i++) {
                        if (cta[i].hasWheelChair()) {
                            System.out.println((cta[i].getName() + " :station has wheelchair access"));
                            count++;
                        }
                    }
                    System.out.println("total: "+ count + " :stations have wheelchair access");
                    break;
                case "n":
                    for (int i = 0; i < cta.length; i++) {
                        if (!cta[i].hasWheelChair()) {
                            System.out.println(cta[i].getName() + " :station does not have wheelchair access ");
                        }
                    }
                    break;
                default:
                    System.out.println("Invalid input");
                    displayByWheelChair(cta);
                    break;
            }

        }catch (Exception e){
            System.out.println("Invalid input");
        }
        input.close();

    }

    public static void displayNearest(CTAStation[] cta){
        Scanner input = new Scanner(System.in);
        try {
            double[] values = new double[cta.length];
            System.out.print("Please enter a latitude: ");
            double latitude = input.nextDouble();
            System.out.print("Please enter a longitude: ");
            double longitude = input.nextDouble();
            if (latitude >= -90 && latitude <=90 && longitude >=-180 && longitude <=180) {
                for (int i = 0; i < cta.length; i++) {
                    double lat = cta[i].getLat();
                    double lng = cta[i].getLng();
                    values[i] = cta[i].calcDistance(lat, latitude, lng, longitude);
                }

            }else {
                System.out.println("Invalid input");
                displayNearest(cta);
            }

            int index = 0;
            for (int i = 1; i < values.length; i++) {
                if (values[index] > values[i] ){
                    index = i;
                }
            }


            System.out.println("--->"+cta[index].getName()+"  station is the nearest station: "+values[index]);

        }catch (Exception e){
            System.out.println("Invalid input");
        }
    }
}
