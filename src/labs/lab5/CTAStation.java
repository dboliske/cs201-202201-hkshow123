package src.labs.lab5;

public class CTAStation extends GeoLocation{
    private String name;
    private String location;
    private Boolean wheelChair;
    private Boolean open;

    public CTAStation(){
        super();
        this.name ="";
        this.location = "";
        this.wheelChair = false;
        this.open = false;
    }
    public CTAStation(String name,double lat,double lng,String location,Boolean wheelChair,Boolean open){
        super(lat, lng);
        this.name = name;
        this.location = location;
        this.wheelChair = wheelChair;
        this.open = open;

    }
    public String getName(){
        return this.name;
    }

    public String getLocation(){
        return this.location;
    }

    public boolean hasWheelChair(){
        return this.wheelChair;
    }

    public boolean isOpen(){
        return this.open;
    }
    public void setName(String name){
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWheelChair(Boolean wheelChair) {
        this.wheelChair = wheelChair;
    }
    public void setOpen(Boolean open){
        this.open = open;
    }
    public String toString(){
        return "name is: "+ this.getName() + " location is: "+ this.location + " has wheelchair: "+ this.wheelChair +
                " is open: " + this.open + " lat is: "+super.getLat() + " lng is: " + super.getLng();
    }
    @Override
    public boolean equals(Object obj){
       if (!super.equals(obj)){
           return false;
       }else if(!(obj instanceof CTAStation)){
           return false;
       }
       CTAStation cta = (CTAStation) obj;
       if (!this.name.equals(((CTAStation) obj).getName())){
           return false;
       }else if (!this.open !=((CTAStation) obj).isOpen()){
           return false;
       }else if (!this.wheelChair !=((CTAStation) obj).hasWheelChair()){
           return false;
       }

       return true;
    }
}
